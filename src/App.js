import React, { Component } from "react";
import todosList from "./todos.json";
import TodoList from "./components/todoList/TodoList"
import { v4 as uuidv4 } from 'uuid';

class App extends Component {
  state = {
    todos: todosList,
    value: ''
  };

  // keep track of input. 
  handleInput = (event) => {
    event.persist();
    this.setState((state) => ({
      value: event.target.value
    }));
  }

  // take the submitted input and add it to todos array.
  handleSubmit = (event) => {
    event.persist();
    event.preventDefault();

    let newTodo = {
      "userId": 1,
      "id": uuidv4(),
      "title": this.state.value,
      "completed": false
    };

    this.setState((state) => {
      return {
        todos: [...state.todos, newTodo],
        value: ''
      }
    });
  }

  // use to update whether a todo item is completed.
  // make a copy of array from state, use copy for comparing id,
  // update completed property on item with matching id,
  // then update state with info from copy.
  handleClick = (event, id) => {
    let newTodos = [...this.state.todos]
    for (let i = 0; i < newTodos.length; i++) {
      if (newTodos[i].id === id) {
        newTodos[i].completed = !newTodos[i].completed
        // use to check that "completed" is being toggled.
        // console.log(newTodos[i])
        break
      }
    }

    this.setState((state) => {
      return {
        todos: [...newTodos]
      }
    });
  }

  // use to delete a todo item from the list.
  handleDelete = (event, id) => {
    for (let i = 0; i < this.state.todos.length; i++) {
      if (this.state.todos[i].id === id) {
        this.state.todos.splice(i, 1)
        // check that todos array has decreased by 1.
        // console.log(this.state.todos)
        break
      }
    }

    this.setState((state) => {
      return {
        todos: [...state.todos]
      }
    });
  }

  // use to remove all completed todos.
  // take the items still marked as "completed: false", 
  // push to a new array called "incomplete",
  // then use that to update todos.
  clearCompleted = () => {
    let incomplete = [];
    for (let i = 0; i < this.state.todos.length; i++) {
      if (!this.state.todos[i].completed) {
        incomplete.push(this.state.todos[i])
      }
    }
    // console.log(incomplete)
    this.setState((state) => {
      return {
        todos: incomplete
      }
    });
  }

  render() {
    return (
      <section className="todoapp" >
        <header className="header">
          <h1>todos</h1>
          <form onSubmit={this.handleSubmit}>
            <input
              className="new-todo"
              placeholder="What needs to be done?"
              autoFocus
              value={this.state.value}
              onChange={this.handleInput}
            />
          </form>
        </header>
        <TodoList
          todos={this.state.todos}
          handleClick={this.handleClick}
          handleDelete={this.handleDelete}
        />
        <footer className="footer">
          <span className="todo-count">
            <strong>0</strong> item(s) left
          </span>
          <button onClick={this.clearCompleted} className="clear-completed">Clear completed</button>
        </footer>
      </section>
    );
  }
}

export default App;