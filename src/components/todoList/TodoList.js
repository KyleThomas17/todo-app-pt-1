import React, { Component } from "react";
import TodoItem from "../todoItem/TodoItem"

class TodoList extends Component {
    render() {
        return (
            <section className="main">
                <ul className="todo-list">
                    {this.props.todos.map((todo) => (
                        <TodoItem
                            key={todo.id}
                            id={todo.id}
                            title={todo.title}
                            completed={todo.completed}
                            handleClick={this.props.handleClick}
                            handleDelete={this.props.handleDelete}
                        />
                    ))}
                </ul>
            </section>
        );
    }
}

export default TodoList;