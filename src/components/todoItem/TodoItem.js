import React, { Component } from "react";

class TodoItem extends Component {
    render() {
        return (
            <li
                className={this.props.completed ? "completed" : ""}>
                <div className="view">
                    <input
                        onClick={(event) => this.props.handleClick(event, this.props.id)}
                        className="toggle"
                        type="checkbox"
                        readOnly
                        checked={this.props.completed}
                    />
                    <label onClick={(event) => this.props.handleClick(event, this.props.id)}>
                        {this.props.title}
                    </label>
                    <button
                        onClick={(event) => this.props.handleDelete(event, this.props.id)}
                        className="destroy"
                    />
                </div>
            </li>
        );
    }
}

export default TodoItem;